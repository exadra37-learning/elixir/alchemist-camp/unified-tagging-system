defmodule Utils.DocTests do
  use ExUnit.Case

  doctest Utils.TagsUnify
  doctest Utils.UniqueCaseInsensitive
end
