defmodule Cms.Repo.Migrations.AddUniqueIndexToTopicsTable do
  use Ecto.Migration

  def change do
    create unique_index(:topics, [:text])
  end
end
