defmodule Cms.Repo.Migrations.CreateEntityLikes do
  use Ecto.Migration

  def change do
    create table(:entity_likes) do
      add :entity_id, references(:entities, on_delete: :nothing)
      add :user_id, references(:users, on_delete: :nothing)

      timestamps()
    end

    create index(:entity_likes, [:entity_id])
    create index(:entity_likes, [:user_id])
    create unique_index(:entity_likes, [:entity_id, :user_id])
  end
end
