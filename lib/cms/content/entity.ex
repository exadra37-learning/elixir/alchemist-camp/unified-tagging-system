defmodule Cms.Content.Entity do
  use Ecto.Schema
  import Ecto.Changeset
  alias Cms.Content.{Article, Entity, Episode, Resource, Topic}
  alias Cms.Users.User

  schema "entities" do
    field :is_premium, :boolean, default: false
    field :is_published, :boolean, default: false
    field :requires_login, :boolean, default: false
    field :view_count, :integer, default: 0
    many_to_many :topics, Topic, join_through: "entity_topics"
    many_to_many :users, User, join_through: "entity_likes"
    has_one :article, Article
    has_one :episode, Episode
    has_one :resource, Resource

    timestamps()
  end

  @doc false
  def changeset(entity, attrs) do
    entity
    |> cast(attrs, [:is_published, :is_premium, :requires_login, :view_count])
    |> validate_required([:is_published, :is_premium, :requires_login, :view_count])
  end
end
