defmodule Cms.Content.EntityLikes do
  use Ecto.Schema
  import Ecto.Changeset
  alias Cms.Content.{Entity, EntityLikes}
  alias Cms.Users.User

  schema "entity_likes" do
    belongs_to :entity, Entity
    belongs_to :user, User

    timestamps()
  end

  @doc false
  def changeset(entity_likes, attrs) do
    entity_likes
    |> cast(attrs, [:entity_id, :user_id])
    |> validate_required([])
  end
end
