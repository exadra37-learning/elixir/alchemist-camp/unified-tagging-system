defmodule Cms.Tags.TagsUtils do

  alias Utils.TagsUnify

  @tags_separator ", "

  # Takes a resource with an Entity preloaded, that also needs to have a Topics
  #  preloaded on it.
  def tags_loaded(%{entity: %{topics: topics}}) do
    topics |> Enum.map_join(@tags_separator, & &1.text)
  end

  def unify_tags(%{"topics" => topics} = attrs) when is_binary(topics) do
    %{attrs | "topics" => TagsUnify.string(topics) }
  end

  def unify_tag(%{"text" => text} = attrs) when is_binary(text) do
    %{ attrs | "text" => TagsUnify.string(text) }
  end

end
