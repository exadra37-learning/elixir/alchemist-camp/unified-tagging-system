defmodule Cms.Tags.Update.UpdateTagsRepo do

  alias Cms.Repo
  alias Utils.TagsUnify
  alias Cms.Tags.TagsUtils
  alias Cms.Tags.Add.AddTagsRepo
  alias Cms.Tags.Remove.RemoveTagsRepo

  def update_tags({:ok, content} = result, %{"topics" => topics}) do
    update_tags(content, topics)
    result
  end

  def update_tags(content, %{"topics" => tags}) do
    content = content |> Repo.preload(entity: [:topics])
    update_tags(content, tags)
  end

  # @TODO maybe extract topics from `topics` in content?
  def update_tags(content, topics) when is_binary(topics) do

    # @TODO Create an helper method in a utils module to split the tags.
    old_tags = TagsUtils.tags_loaded(content) |> TagsUnify.string_to_list()
    new_tags = topics |> TagsUnify.string_to_list()

    # Using the `--` operator to get all the `new_tags` not present in the
    #  `old_tags`, and call `add_tag()` to create in the database the
    #  association between the content, aka entity, and the new tag.
    content
    |> AddTagsRepo.insert_tags(new_tags -- old_tags)
    |> RemoveTagsRepo.delete_tags(old_tags -- new_tags)
  end
end
