defmodule Cms.Tags.Add.AddTagsRepo do

  alias Cms.Repo
  alias Cms.Content.Topic
  alias Cms.Content.Entity
  alias Cms.Content.EntityTopic
  alias Utils.TagsUnify

  def insert({:ok, content} = result, %{"topics" => topics}) do
    insert_tags(content, topics)
    result
  end

  def insert_tag(entity, topic_text) when is_binary(topic_text) do

    topic_text = TagsUnify.string(topic_text)

    topic =
      case Repo.get_by(Topic, %{text: topic_text}) do
        nil ->
          %Topic{} |> Topic.changeset(%{text: topic_text}) |> Repo.insert!()
        topic ->
          topic
      end

    insert_tag(entity, topic.id)
  end

  def insert_tag(%{entity_id: eid}, tid) when is_number(tid) do
    insert_tag(eid, tid)
  end

  def insert_tag(%Entity{} = entity, topic_id) when is_number(topic_id) do
    insert_tag(entity.id, topic_id)
  end

  def insert_tag(eid, tid) do
    EntityTopic.changeset(%EntityTopic{}, %{entity_id: eid, topic_id: tid})
    |> Repo.insert()
  end

  def insert_tags(content, tags) when is_binary(tags) do
    tags
    |> TagsUnify.string_to_list()
    |> Enum.each(&insert_tag(content, &1))
    content
  end

  def insert_tags(content, tags) when is_list(tags) do
    tags
    |> Enum.each(&insert_tag(content, &1))
    content
  end

end
