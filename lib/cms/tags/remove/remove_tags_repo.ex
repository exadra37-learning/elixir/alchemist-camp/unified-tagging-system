defmodule Cms.Tags.Remove.RemoveTagsRepo do

  alias Cms.Repo
  alias Cms.Content.Topic
  alias Cms.Content.Entity
  alias Cms.Content.EntityTopic
  alias Utils.TagsUnify

  def delete_tag(entity, topic_text) when is_binary(topic_text) do
    topic_text = TagsUnify.string(topic_text)

    case Repo.get_by(Topic, %{text: topic_text}) do
      nil -> nil
      topic -> delete_tag(entity, topic.id)
    end
  end

  def delete_tag(%{entity_id: eid}, tid) when is_number(eid) and is_number(tid) do
    delete_tag(eid, tid)
  end

  def delete_tag(%Entity{} = entity, topic_id) when is_number(topic_id) do
    delete_tag(entity.id, topic_id)
  end

  def delete_tag(eid, tid) when is_number(eid) and is_number(tid) do
    case Repo.get_by(EntityTopic, %{entity_id: eid, topic_id: tid}) do
      nil -> nil
      tag -> Repo.delete(tag)
    end
  end

  def delete_tag(entity, topic_id) do
    IO.inspect entity
    IO.inspect topic_id
    raise "Failed to match params for delete_tag/2 ."
  end

  def delete_tags(content, tags) when is_binary(tags) do
    tags
    |> Enum.each(&delete_tag(content, &1))
  end

  def delete_tags(content, tags) when is_list(tags) do
    tags
    |> Enum.each(&delete_tag(content, &1))
  end

  def delete_tags(content, tags) do
    IO.inspect content
    IO.inspect tags
    raise "Failed to match params for delete_tags/2 ."
  end
end
