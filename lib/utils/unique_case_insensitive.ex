defmodule Utils.UniqueCaseInsensitive do

  @moduledoc """
  Module to take a string or list and return it without duplicates in a case
  insensitive way.

  To bear in mind that `"Elixir, Elixir., Elixir ."` will have each of its words
  to be considered as unique, but if you want each word to be considered has the
  same word, then you are better using `Utils.TagsUnify.string()`.

  ## Examples

      iex> "MyELixirStatus, my elixir status, Phoenix, Phoenix" |> Utils.UniqueCaseInsensitive.string()
      "MyELixirStatus, my elixir status, Phoenix"

      iex> "MyELixirStatus, my elixir status, Phoenix, Phoenix." |> Utils.UniqueCaseInsensitive.string()
      "MyELixirStatus, my elixir status, Phoenix, Phoenix."

      iex> "MyELixirStatus, my elixir status, Phoenix, Phoenix ." |> Utils.UniqueCaseInsensitive.string()
      "MyELixirStatus, my elixir status, Phoenix, Phoenix ."
  """

  @doc """
  Remove all duplicate words from a string, by preserving the first word in a
  case insensitive way. By default separates the word by a comma.

  ## Examples

      iex> Utils.UniqueCaseInsensitive.test_string |> Utils.UniqueCaseInsensitive.string()
      "Elixir, MyElixirStatus, My Elixir Status"
  """
  def string(value, separator \\ ",") when is_binary(value) do
    value
    #|> String.replace(~r/\W+/, "") # Only words?
    |> String.split(separator)
    |> _unique_list()
    |> Enum.join(separator)
  end

  @doc """
  Remove all duplicate words from the given list in a case insensitive way.

  ## Examples

      iex> Utils.UniqueCaseInsensitive.test_string_list |> Utils.UniqueCaseInsensitive.list()
      ["Elixir", "MyElixirStatus", "My Elixir Status"]
  """
  def list(values) when is_list(values) do
    _unique_list(values)
    |> Enum.map(&String.trim(&1))
  end

  # @link https://www.rosettacode.org/wiki/Remove_duplicate_elements#Elixir
  defp _unique_list(values) when is_list(values) do
    values
    |> _unique([], [])
  end

  defp _unique([], _tags, unique_tags) do
    Enum.reverse(unique_tags)
  end

  defp _unique([head | tail], tags, unique_tags) do
    tag =
      head
      |> String.trim()
      |> String.replace(~r/\s+/, " ") # "My   Elixir Status" |> "My Elixir Status"
      |> String.downcase()

    case tag in tags do
      true ->
        _unique(tail, tags, unique_tags)
      _ ->
        _unique(tail, [tag | tags], [head | unique_tags])
    end
  end

  @test_string "Elixir, elixir, MyElixirStatus, myelixirstatus, myElixirStatus, My Elixir Status, My   Elixir Status"
  @doc false
  def test_string(), do: @test_string
  @doc false
  def test_string_list(),   do: @test_string |> String.split(",")
end
