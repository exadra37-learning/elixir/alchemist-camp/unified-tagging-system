defmodule Utils.TagsUnify do

  @moduledoc """
  This module unifies the given tags, by removing duplicates and merging the
  ones that are similar.

  ## Examples

      iex> "ELIXIR" |> Utils.TagsUnify.string()
      "Elixir"

      iex> "MyElixirStatus " |> Utils.TagsUnify.string()
      "MyElixirStatus"

      iex> "myElixirstatus " |> Utils.TagsUnify.string()
      "MyElixirstatus"

      iex> "myElixirStatus " |> Utils.TagsUnify.string()
      "MyElixirStatus"

      iex> "my-elixir-status " |> Utils.TagsUnify.string()
      "MyElixirStatus"

      iex> "My_ELIXIR_status" |> Utils.TagsUnify.string()
      "MyElixirStatus"

      iex> "myelixirstatus " |> Utils.TagsUnify.string()
      "Myelixirstatus"

      iex> "MYELIXIRSTATUS" |> Utils.TagsUnify.string()
      "Myelixirstatus"

      iex> "PHOENIX LIVE VIEW" |> Utils.TagsUnify.string()
      "PhoenixLiveView"

      iex> "Phoenix-framework" |> Utils.TagsUnify.string()
      "PhoenixFramework"

      iex> "My   Elixir Status, my-elixir-status, myelixirstatus,,, MyElixirStatus, , " |> Utils.TagsUnify.string()
      "MyElixirStatus"

      iex> Utils.TagsUnify.test_tags_list()
      [
          " elixir",
          " My   Elixir Status",
          "my     elixir    Status",
          " phoenix   framework ",
          " phoenixframework",
          " phoenix ",
          "   ",
          "   ",
          " Elixir ",
          " My-Elixir-Status",
          "myelixirstatus",
          "MyELixirStatus",
          "my-elixir-status",
          "myElixirStatus",
          " my Elixir status",
          " my elixir status",
          " Elixir",
          "ELIXIR",
          "NERVES",
          "PHOENIX LIVE VIEW",
      ]

      iex> Utils.TagsUnify.test_tags_list() |> Utils.TagsUnify.list()
      [
          "Elixir",
          "MyElixirStatus",
          "PhoenixFramework",
          "Phoenix",
          "Nerves",
          "PhoenixLiveView"
      ]
  """

  alias Utils.TagsUnify
  alias Utils.UniqueCaseInsensitive

  defstruct [
    split_separator: ",",
    join_separator: ", ",
    replace_chars: "_-"
  ]

  @doc """
  Unify the given string of tags, without duplicates, and with similar tags
  being merged.

  ## Examples:

      iex> Utils.TagsUnify.test_tags_string |> Utils.TagsUnify.string
      "Elixir, MyElixirStatus, PhoenixFramework, Phoenix, Nerves, PhoenixLiveView"
  """
  def string(tags, %TagsUnify{} = opts \\ %TagsUnify{}) when is_binary(tags) do
    tags
    |> string_to_list(opts)
    |> Enum.join(opts.join_separator)
  end

  @doc """
  Unify the given string of tags into a list, without duplicates, and with
  similar tags being merged.

  ## Examples

      iex> Utils.TagsUnify.test_tags_string |> Utils.TagsUnify.string_to_list()
      ["Elixir", "MyElixirStatus", "PhoenixFramework", "Phoenix", "Nerves", "PhoenixLiveView"]
  """
  def string_to_list(tags, %TagsUnify{} = opts \\ %TagsUnify{}) when is_binary(tags) do
    tags
    |> String.split(~r[#{opts.split_separator}], trim: true)
    |> _unify_tags(opts)
  end

  @doc """
  Unify the given list of tags, without duplicates, and with similar tags being
  merged.

  ## Examples

      iex> Utils.TagsUnify.test_tags_list |> Utils.TagsUnify.list()
      ["Elixir", "MyElixirStatus", "PhoenixFramework", "Phoenix", "Nerves", "PhoenixLiveView"]
  """
  def list(tags, %TagsUnify{} = opts \\ %TagsUnify{}) when is_list(tags) do
    tags
    |> _unify_tags(opts)
  end

  @doc """
  Unify the given list of tags into a string, without duplicates, and with
  similar tags being merged.

  ## Examples

      iex> Utils.TagsUnify.test_tags_list |> Utils.TagsUnify.list_to_string()
      "Elixir, MyElixirStatus, PhoenixFramework, Phoenix, Nerves, PhoenixLiveView"
  """
  def list_to_string(tags, %TagsUnify{} = opts \\ %TagsUnify{}) when is_list(tags) do
    tags
    |> _unify_tags(opts)
    |> Enum.join(", ")
  end

  defp _unify_tags(tags, %TagsUnify{} = opts) when is_list(tags) do
    tags
    |> Enum.map(&_unify_tag(&1, opts))
    |> UniqueCaseInsensitive.list()
    |> Enum.reject(fn tag -> String.length(tag) == 0 end)
  end

  defp _unify_tag(tag, %TagsUnify{} = opts) when is_binary(tag) do
    tag
    |> String.replace(~r/\s+/, " ")
    |> _replace_chars(opts.replace_chars, " ")
    |> _title_case()

    # @TODO maybe pluralize or singularize?
    #   + @link https://hexdocs.pm/inflex/1.8.0/Inflex.html
    #   + @link https://hex.pm/packages?_utf8=%E2%9C%93&search=inflex&sort=recent_downloads
    #   + @link https://hex.pm/packages?search=inflector&sort=recent_downloads
  end

  # @link https://www.rosettacode.org/wiki/Strip_a_set_of_characters_from_a_string#Elixir
  defp _replace_chars(str, match, replace) do
    String.replace(str, ~r/[#{match}]/, replace)
  end

  # @link https://kozmicluis.com/title-case-capitalize-sentence/
  defp _title_case(words) do

    # Given words like `my elixir status` we want to get back `MyElixirStatus`.
    words
      |> String.split
      |> Stream.map(&_upcase_first_char/1)
      |> Stream.map(&_capitalize_word/1)
      |> Enum.join("")
  end

  defp _capitalize_word(word) do

    # When the given `words` is all upper case, like `ELIXIR`, the `word` will
    #  still be all upper case, thus we need to hack our way around it, in order
    #  to return the word correctly formatted, like `Elixir`.
    case String.upcase(word) == word do
      true ->
        String.capitalize(word)
      _ ->
        word
    end
  end

  # @link https://stackoverflow.com/a/58677092/6454622
  defp _upcase_first_char(<<first::utf8, rest::binary>>) do
    String.upcase(<<first::utf8>>) <> rest
  end

  @test_tags_list [
    " elixir",
    " My   Elixir Status",
    "my     elixir    Status",
    " phoenix   framework ",
    " phoenixframework",
    " phoenix ",
    "   ",
    "   ",
    " Elixir ",
    " My-Elixir-Status",
    "myelixirstatus",
    "MyELixirStatus",
    "my-elixir-status",
    "myElixirStatus",
    " my Elixir status",
    " my elixir status",
    " Elixir",
    "ELIXIR",
    "NERVES",
    "PHOENIX LIVE VIEW",
  ]

  @doc false
  def test_tags_string(), do: @test_tags_list |> Enum.join(", ")
  @doc false
  def test_tags_list(),   do: @test_tags_list
end
