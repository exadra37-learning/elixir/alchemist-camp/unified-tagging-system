defmodule CmsWeb.Router do
  use CmsWeb, :router
  use Pow.Phoenix.Router
  use Pow.Extension.Phoenix.Router, otp_app: :cms

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :protected do
    plug Pow.Plug.RequireAuthenticated, error_handler: Pow.Phoenix.PlugErrorHandler
  end


  scope "/" do
    pipe_through :browser

    pow_routes()
    pow_extension_routes()
  end


  scope "/", CmsWeb do
    pipe_through [:browser, :protected]

    resources "/articles", ArticleController
    resources "/episodes", EpisodeController
    resources "/resources", ResourceController
    resources "/topics", TopicController
    get "/tagged/:topic", TopicController, :tagged
  end

  scope "/", CmsWeb do
    pipe_through :browser

    get "/", PageController, :index
  end
  # Other scopes may use custom stacks.
  # scope "/api", CmsWeb do
  #   pipe_through :api
  # end
end
