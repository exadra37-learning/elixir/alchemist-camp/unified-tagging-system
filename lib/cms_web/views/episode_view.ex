defmodule CmsWeb.EpisodeView do
  use CmsWeb, :view

  # Urls can be like:
  #   + https://youtu.be/0jzcPnsE4nQ
  #   + https://www.youtube.com/watch?v=0jzcPnsE4nQ
  def parse_video_url(episode) do
    video_id = video_id(episode)
    #url = String.replace(episode.video_url, ~r(\.be), "be.com/embed")

    "https://youtube.com/embed/" <> video_id <> "?rel=0"
  end

  def video_id(episode) do
    ~r{^.*(?:youtu\.be/|\w+/|v=)(?<id>[^#&?]*)}
    |> Regex.named_captures(episode.video_url)
    |> get_in(["id"])
  end
end
