defmodule CmsWeb.LayoutView do
  use CmsWeb, :view

  def meta_title(%{meta: %{title: title}}), do: title
  def meta_title(%{meta: %{edit_title: title}}), do: "Edit: #{title}"
  def meta_title(_assigns), do: "CMS for DEVs"

  def meta_author(%{meta: %{author: author}}), do: author
  def meta_author(_assigns), do: "CMS for DEVs"

  def meta_description(%{meta: %{description: description}}), do: description
  def meta_description(_assigns), do: "A CMS to help developers with their day to day work-flow. Like learning new technologies, build a knowledge database, and keeps tabs on what's going on."

  #def meta_generator(%{meta: %{generator: generator}}), do: generator
  def meta_generator(_assigns), do: "CMS for DEVs"

  def meta_url(%{meta: %{url: url}}), do: url
  def meta_url(%{conn: conn}), do: Plug.Conn.request_url(conn)

  def meta_updated_time(%{meta: %{updated_time: updated_time}}), do: updated_time
  # @TODO check correct format to use here.
  # Current `2019-12-09 18:50:33.188869Z`, but Exadra37 Blog uses `2016-07-22T08:46:47+01:00`.
  def meta_updated_time(_assigns), do: DateTime.utc_now

end
