defmodule CmsWeb.ResourceController do
  use CmsWeb, :controller

  alias Cms.Content
  alias Cms.Content.Resource
  alias Cms.Tags.TagsUtils

  def index(conn, _params) do
    resources = Content.list_resources()
    render(conn, "index.html", resources: resources, meta: %{title: "Listing Resources"})
  end

  def new(conn, _params) do
    changeset = Content.change_resource(%Resource{})
    render(conn, "new.html", changeset: changeset, tags: [], meta: %{title: "New Resource"})
  end

  def create(conn, %{"resource" => %{"topics" => topics} = params}) do
    case Content.create_resource(params) do
      {:ok, resource} ->
        conn
        |> put_flash(:info, "Resource created successfully.")
        |> redirect(to: Routes.resource_path(conn, :show, resource))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset, tags: topics, meta: %{title: "New Resource"})
    end
  end

  def show(conn, %{"id" => id}) do
    resource = Content.get_resource!(id)
    tags = TagsUtils.tags_loaded(resource)
    render(conn, "show.html", resource: resource, tags: tags, meta: %{title: resource.title})
  end

  def edit(conn, %{"id" => id}) do
    resource = Content.get_resource!(id)
    tags = TagsUtils.tags_loaded(resource)
    changeset = Content.change_resource(resource)
    render(conn, "edit.html", resource: resource, changeset: changeset, tags: tags, meta: %{edit_title: resource.title})
  end

  def update(conn, %{"id" => id, "resource" => %{"topics" => topics} = params}) do
    resource = Content.get_resource!(id)

    case Content.update_resource(resource, params) do
      {:ok, resource} ->
        conn
        |> put_flash(:info, "Resource updated successfully.")
        |> redirect(to: Routes.resource_path(conn, :show, resource))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", resource: resource, changeset: changeset, tags: topics, meta: %{edit_title: resource.title})
    end
  end

  def delete(conn, %{"id" => id}) do
    resource = Content.get_resource!(id)
    {:ok, _resource} = Content.delete_resource(resource)

    conn
    |> put_flash(:info, "Resource deleted successfully.")
    |> redirect(to: Routes.resource_path(conn, :index))
  end
end
