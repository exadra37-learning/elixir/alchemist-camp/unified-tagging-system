defmodule CmsWeb.EpisodeController do
  use CmsWeb, :controller

  alias Cms.Content
  alias Cms.Content.Episode
  alias Cms.Content.Topic
  alias Cms.Tags.TagsUtils

  def index(conn, _params) do
    episodes = Content.list_episodes()
    render(conn, "index.html", episodes: episodes, meta: %{title: "Listing Episodes"})
  end

  def new(conn, _params) do
    changeset = Content.change_episode(%Episode{})
    render(conn, "new.html", changeset: changeset, tags: [], meta: %{title: "New Episode"})
  end

  def create(conn, %{"episode" => %{"topics" => topics} = params}) do
    case Content.create_episode(params) do
      {:ok, episode} ->
        conn
        |> put_flash(:info, "Episode created successfully.")
        |> redirect(to: Routes.episode_path(conn, :show, episode))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset, tags: topics, meta: %{title: "New Episode"})
    end
    conn
  end

  def show(conn, %{"id" => id}) do
    episode = Content.get_episode!(id)
    tags = TagsUtils.tags_loaded(episode)
    render(conn, "show.html", episode: episode, tags: tags, meta: %{title: episode.title})
  end

  def edit(conn, %{"id" => id}) do
    episode = Content.get_episode!(id)
    tags = TagsUtils.tags_loaded(episode)
    changeset = Content.change_episode(episode)
    render(conn, "edit.html", episode: episode, changeset: changeset, tags: tags, meta: %{edit_title: episode.title})
  end

  def update(conn, %{"id" => id, "episode" => %{"topics" => topics} = params}) do

    # @TODO Not a Controller responsibility to deal with this section
    episode = Content.get_episode!(id)

    case Content.update_episode(episode, params) do
      {:ok, episode} ->
        conn
        |> put_flash(:info, "Episode updated successfully.")
        |> redirect(to: Routes.episode_path(conn, :show, episode))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", episode: episode, changeset: changeset, tags: topics, meta: %{edit_title: episode.title})
    end
  end

  def delete(conn, %{"id" => id}) do
    episode = Content.get_episode!(id)
    {:ok, _episode} = Content.delete_episode(episode)

    conn
    |> put_flash(:info, "Episode deleted successfully.")
    |> redirect(to: Routes.episode_path(conn, :index))
  end

end
