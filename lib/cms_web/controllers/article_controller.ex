defmodule CmsWeb.ArticleController do
  use CmsWeb, :controller

  alias Cms.Content
  alias Cms.Content.Article
  alias Cms.Tags.TagsUtils

  def index(conn, _params) do
    articles = Content.list_articles()
    render(conn, "index.html", articles: articles, meta: %{title: "Listing Articles"})
  end

  def new(conn, _params) do
    changeset = Content.change_article(%Article{})
    render(conn, "new.html", changeset: changeset, tags: [], meta: %{title: "New Article"})
  end

  def create(conn, %{"article" => %{"topics" => topics} = params}) do
    case Content.create_article(params) do
      {:ok, article} ->
        conn
        |> put_flash(:info, "Article created successfully.")
        |> redirect(to: Routes.article_path(conn, :show, article))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset, tags: topics, meta: %{title: "New Article"})
    end
  end

  def show(conn, %{"id" => id}) do
    article = Content.get_article!(id)
    tags = TagsUtils.tags_loaded(article)
    render(conn, "show.html", article: article, tags: tags, meta: %{title: article.title})
  end

  def edit(conn, %{"id" => id}) do
    article = Content.get_article!(id)
    tags = TagsUtils.tags_loaded(article)
    changeset = Content.change_article(article)
    render(conn, "edit.html", article: article, changeset: changeset, tags: tags, meta: %{edit_title: article.title})
  end

  def update(conn, %{"id" => id, "article" => %{"topics" => topics} = params}) do
    article = Content.get_article!(id)

    case Content.update_article(article, params) do
      {:ok, article} ->
        conn
        |> put_flash(:info, "Article updated successfully.")
        |> redirect(to: Routes.article_path(conn, :show, article))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", article: article, changeset: changeset, tags: topics, meta: %{edit_title: article.title})
    end
  end

  def delete(conn, %{"id" => id}) do
    article = Content.get_article!(id)
    {:ok, _article} = Content.delete_article(article)

    conn
    |> put_flash(:info, "Article deleted successfully.")
    |> redirect(to: Routes.article_path(conn, :index))
  end
end
